# rpi-scripts
A collection of scripts I use on my Raspberry Pi which I find "useful".
> Make sure to run `sudo chmod +x <script name>` to make the script executable before running them~!

> Also you need tclsh installed to run .tcl files~!

> Also please note that none of these scripts are guaranteed to work so use them with caution.

## Command usage
A guide on using these scripts.

## fail2pi
**Script filename**: `fail2pi.sh`

**Description**: A script which aids in managing `fail2ban` and `iptables` on the Pi.

**Dependencies**:
```
- Root access or sudo privileges.
```
**Usage**:
```
$ ./fail2pi.sh <logs|list|unban> [ip to unban]
$ ./fail2pi.sh logs # returns logs from fail2ban
$ ./fail2pi.sh list # returns list of banned IPs
$ ./fail2pi.sh unban 127.0.0.1 # unbans 127.0.0.1 from fail2ban
```

## goodbye
**Script filename**: `goodbye.sh`

**Description**: A script to ease the process of rebooting and shutting the Pi down.

**Dependencies**:
```
- Root access or sudo privileges.
```
**Usage**:
```
$ ./goodbye.sh reboot # reboots the pi
$ ./goodbye.sh shutdown # shuts the pi down
```

## motd
**Script filename**: `motd.tcl`

**Description**: A script which provides basic system information regarding the Pi when you login via SSH.

**Dependencies**:
```
- tclsh
- Disabled motd
```
**Usage**:
> Usually this auto-runs when you successfully login to your user account via ssh but you can manually run it like this
```
$ ./motd.tcl
```

## pibackup
**Script filename**: `pibackup.sh`

**Description**: A script which eases the process of backing up and restoring the Pi.

**Dependencies**:
```
- gzip
- Root access or sudo privileges.
- Properly configured RPI_HOSTNAME, RPI_USER, RPI_PORT, and RPI_PRIVATE_KEY_PATH environment variables.
```
**Usage**:
> Note that this script **must** be run on the local PC, **not** on the Pi!
```
$ ./pibackup.sh backup # backs up the pi 
$ ./pibackup.sh restore # restores the backup
```

## piupdate
**Script filename**: `piupdate.sh`

**Description**: A script which eases the process of upgrading the Raspberry Pi to the latest version of Raspbian.

**Dependencies**:
```
- Root access or sudo privileges.
```

**Usage**:
```
$ ./piupdate.sh
```

## rotate
**Script filename**: `rotate.sh`

**Description**: Creates a rotating GIF animation from a still PNG image.

**Dependencies**:
```
- Gifsicle
```

**Usage**:
```
$ ./rotate.sh <input PNG file> <output file to save the GIF to>
```