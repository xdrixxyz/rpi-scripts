#!/bin/bash
# this script is intended to be ran ON YOUR LOCAL COMPUTER
# not on the raspberry pi!

# run checks to see if we are running this on the Pi or on a local pc
neofetch | grep 'Raspbian' &> /dev/null
if [ $? == 0 ]; then
   echo "You need to run this script on a local machine, not on the Pi!"
   exit 1;
fi

SUDO='' # set sudo variable so we can run the script as sudo in case we're not root
if [ $EUID -ne 0 ]; then
  echo "You are not root, running as sudo..."
  SUDO='sudo' # set sudo equal to the sudo command
fi

if [ $# -eq 0 ]; then # if we didnt specify args
  echo "Please specify either 'backup' or 'restore' as an argument."
  exit 1
fi

if [ -z $1 ]; then # if the first arg is empty or invalid
  echo "Please specify either 'backup' or 'restore' as an argument."
  exit 1
fi

# environment variable checks
${RPI_USER:?"Please set the RPI_USER environment variable to the user to login as via SSH."}
${RPI_HOSTNAME:?"Please set the RPI_HOSTNAME environment variable to your Raspberry Pi's hostname."}
${RPI_PRIVATE_KEY_PATH:?"Please set the RPI_PRIVATE_KEY_PATH environment variable to the path of your SSH private key (the one which doesn't end in .ppk)"}
${RPI_PORT:?"Please set the RPI_PORT environment variable to the port of the SSH server running on your Raspberry Pi."}

# if our first arg is backup meaning we should backup
if [ $1 == "backup" ]; then
  # connect to our pi via ssh
  $SUDO ssh $RPI_USER@$RPI_HOSTNAME -p $RPI_PORT -i $RPI_PRIVATE_KEY "sudo dd if /dev/mmcblk0 bs=1M | gzip -" | dd of=~/pibackup.gz && echo "Backup saved." && exit || echo "Failed." && exit 1;
fi

# if our first arg is restore meaning we should restore a backup
if [ $1 == "restore" ]; then
  # mount the backup file
  $SUDO diskutil unmountDisk /dev/disk && gzip -dc ~/pibackup.gz | $SUDO dd of /dev/rdisk bs=1m conv=noerror,sync && echo "Restored." && exit || echo "Failed." && exit 1;
fi
