#!/usr/bin/env bash
# usage: ./rotate in_file_name out_file_name

in_file="$1"
out_file="$2"
turns=12
degree=$((360 / $turns));
rotated=()

for (( i=0; i<$turns; i++ )); do
  tempfile=$(mktemp)
  rotated+=("$tempfile")
  rotation=$(($degree * $i))
  convert -background white "$in_file" -distort SRT -$rotation "$tempfile"
done

convert -delay 10 -loop 0 "${rotated[@]}" "$out_file"