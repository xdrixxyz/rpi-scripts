#!/bin/bash
SUDO=''
if [ $EUID -ne 0 ]; then
  echo "You are not root, so I will run this as sudo..."
  SUDO='sudo'
fi

if [[ $# -eq 0 ]]; then
  echo "Please specify either 'logs', 'list', or 'unban' as an argument."
  exit 1
fi

if [[ $1 == "logs" ]]; then
  $SUDO cat /var/log/auth.log
  exit
fi

if [[ $1 == "list" ]]; then
  $SUDO iptables -L -n --line
  exit
fi

if [[ $1 == "unban" ]]; then
  if [[ $# -eq 1 ]]; then
    echo "Please specify the IP to unban from fail2ban."
    exit 1
  fi
  if [ -z "$2" ]; then
    echo "Please specify the IP to unban from fail2ban."
    exit 1
  fi
  $SUDO iptables -D fail2ban-ssh "$2" && printf "Unbanned IP $2." && exit || echo "Failed to unban that IP." && exit 1
fi
