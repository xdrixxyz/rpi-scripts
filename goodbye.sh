#!/bin/bash
SUDO=''
if [ $EUID -ne 0 ]; then
  echo "You are not root, so I will run this script as sudo..."
  SUDO='sudo'
fi

if [[ $# -eq 0 ]]; then
  echo "No arguments specified. Please specify either 'reboot' or 'shutdown'."
  exit 1;
fi

if [[ -z $1 ]]; then
  echo "Invalid argument passed. Please specify either 'reboot' or 'shutdown'."
  exit 1;
fi

if [[ $1 == "reboot" ]]; then
  echo "Rebooting..."
  $SUDO reboot
  exit
fi

if [[ $1 == "shutdown" ]]; then
  echo "Shutting down..."
  $SUDO shutdown -h now
  exit
fi
