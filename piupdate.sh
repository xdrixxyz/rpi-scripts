#!/bin/bash
SUDO=''
if [ $EUID -ne 0 ]; then
  echo "You are not root, so I will run this script as sudo..."
  SUDO='sudo'
fi

$SUDO apt-get update -y && $SUDO apt-get upgrade -y && $SUDO apt-get dist-upgrade -y && echo "Done. Now rebooting machine..." && $SUDO reboot && exit || echo "Failed." && exit 1;
